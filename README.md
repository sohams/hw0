# HW0

Non-graded practice HW

## Goals

The goal of this homework assignment is to get a feel for the basic tools used in this course to complete the homework.

This assignment will involve:
- using [`git`](https://git-scm.com/docs/git): [clone](https://git-scm.com/docs/git-clone), [add](https://git-scm.com/docs/git-add), [commit](https://git-scm.com/docs/git-commit), [push](https://git-scm.com/docs/git-push)
- creating and editing files using a command line text editor
- transfering files between your local computer and FarmShare using [`rsync`](https://linux.die.net/man/1/rsync)/[`scp`](https://linux.die.net/man/1/scp)/[pscp](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- using [`zip`](https://linux.die.net/man/1/zip) to create zip files

## Instructions

1. Use `git` to clone this repository to a space on your FarmShare [home directory](https://en.wikipedia.org/wiki/Home_directory)
    - short [git tutorial](https://www.tutorialspoint.com/git/git_basic_concepts.htm)
    - encourage the use of ssh keys, review the "FarmShare & Gitlab Basics" Canvas Page
2. Create a blank [markdown file](https://www.markdownguide.org/getting-started) called `HW0.md` in this repository using a text editor ([nano](https://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/) is a simple to use text editor)
3. In this new file, answer the following questions using [markdown syntax](https://www.markdownguide.org/basic-syntax) (use no more than four/five sentences for each question):
    1. What excites you about this course?
    2. What worries you about this course?
    3. What one skill are you most hoping to learn and why? 
    4. What role do you think data management will play in your career?
    5. What type of dataset are you hoping to work with this quarter and why?
4. Add the file to the repository and commit the changes to your local repository
5. Push a copy of the local repository to a private repository in your Stanford GitLab space.
5. Print a PDF version of the rendered markdown from the view of your repository on Stanford GitLab.
6. Copy the PDF into your working directory on FarmShare and add/commit/push it
    - use `rsync`/`scp` (Mac) or [pscp](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) (Windows)
7. create a zip file of your repository on FarmShare, excluding .git directory
    - `zip -r <zip file to create> <path to directory to zip> -x '*.git*'`
8. Copy this zip file to your local computer
9. Submit this zip file for HW0 to eduflow.com
